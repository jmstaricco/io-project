use minigrep;

#[test]
fn case_insensitive() {
    let query = "rUsT";
    let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

    assert_eq!(
        vec!["Rust:", "Trust me."],
        minigrep::search_case_insensitive(query, contents)
    );
}

#[test]
#[should_panic(expect="SHOULD PANIC")]
#[ignore]
fn failing_test() {
    let query = "rust";
    let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";
    let res = minigrep::search(query, contents);
    if res == vec!["Trust me."] {
        panic!("SHOULD PANIC");
    }
}

#[test]
fn not_present_case() {
    let query = "not present";
    let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";
    let res = minigrep::search(query, contents);
    assert_eq!(res.len(), 0);
}


